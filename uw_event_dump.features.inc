<?php

/**
 * @file
 * uw_event_dump.features.inc
 */

/**
 * Implements hook_views_api().
 */
function uw_event_dump_views_api() {
  return array("version" => "3.0");
}
